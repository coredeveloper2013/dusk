<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class VisualComfortTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('https://www.visualcomfort.com/trade-portal/')
                ->assertSee('New Customers')
                ->type('#ContentPlaceHolderDefault_Content_Login_3_TxtLogin', '12720CD')
                ->type('#ContentPlaceHolderDefault_Content_Login_3_TxtPassword', 'Success7')
                ->click('#ContentPlaceHolderDefault_Content_Login_3_BtnLogin')
                ->assertUrlIs('https://www.visualcomfort.com/my-account/');
        });
    }
}
